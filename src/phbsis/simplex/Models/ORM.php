<?php

namespace Simplex\Models;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class ORM
{
    use \App\Config\Database;
    
    protected $entityManager;
    
    public function __construct() {
        $this->run();
    }

    public function run()
    {
        $config = Setup::createAnnotationMetadataConfiguration($this->entitiesDir, $this->isDevMode);
        $this->entityManager = EntityManager::create($this->paramsConnect, $config);
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }
    
    public function setEntitiesDir($path)
    {
        $this->entitiesDir = [$path];
    }
    
    protected function responseJson($values)
    {
        $json = [];
        foreach ($values as $val) {
            $json[] = $val->tuple();
        }
        return json_encode($json);
    }
}
