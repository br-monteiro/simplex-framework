<?php

namespace Simplex\Models;

interface ModelInterface
{
    public function __construct();
    
    public function model();
    
    public function find($id);
    
    public function findAll();
    
    public function create(array $values);
    
    public function update($id, array $values);
    
    public function delete($id);
}
