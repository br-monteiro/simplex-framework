<?php

namespace Simplex\Views;

use Simplex\Views\Views;

class ErrorHttp
{
    private $errorStatus;
    private $errorMessage;
    private $errorTemplate;
    private $errorHeader;
    private $errorJson;
    
    public function __construct(array $erro)
    {
        $this->errorStatus = isset($erro['status']) ? $erro['status'] : null;
        $this->errorMessage = isset($erro['message']) ? $erro['message'] : null;
        $this->errorTemplate = isset($erro['template']) ? $erro['template'] : null;
        $this->errorHeader = isset($erro['header']) ? $erro['header'] : null;
        $this->errorJson = isset($erro['json']) ? $erro : false;
        $this->configMessage();
    }
    
    public function configMessage()
    {
        if ($this->errorHeader) {
            header($this->errorHeader);
        }
        
        if ($this->errorJson) {
            echo json_encode($this->errorJson);
            exit();
        }
        
        if ($this->errorMessage && $this->errorStatus && !$this->errorTemplate) {
            echo $this->errorMessage . " - " . $this->errorStatus;
            exit();
        }
        
        if ($this->errorTemplate) {
            $views = new Views();
            
            $value['status'] = $this->errorStatus;
            $value['message'] = $this->errorMessage;
            $views->render($this->errorTemplate, $value);
            exit();
        }
    }
}
