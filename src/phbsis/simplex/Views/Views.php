<?php

namespace Simplex\Views;

use Philo\Blade\Blade;

class Views extends Blade
{
    public function __construct()
    {
        parent::__construct(DIR_VIEWS, DIR_CACHE);
    }
    
    final public function render($view, array $value = [])
    {
        echo $this->view()->make($view, $value)->render();
    }
}
