<?php
namespace Simplex\System;

use Simplex\System\Router;
use Simplex\Controllers\ControllerInterface;
use Simplex\Views\ErrorHttp;

class Bootstrap
{
    private $controller;
    private $action;
    
    private $routerProvider;
    
    public function __construct()
    {
        if (!$this->routerProvider) {
            $this->routerProvider = new Router();
            $this->setController();
            $this->setAction();
        }
    }
    
    public function getParams($key = null)
    {
        $params = $this->routerProvider->getParams();
        
        if ($key) {
            if (array_key_exists($key, $params)) {
                return $params[$key];
            }
            return null;
        }
        
        return $params;
    }
    
    private function setController()
    {
        $value = $this->getParams('controller');
        $this->controller = $value ? : 'IndexController';
    }
    
    private function setAction()
    {
        $value = $this->getParams('action');
        $this->action = $value ? : 'indexAction';
    }
    
    public function run()
    {
        $controller = $this->controller;
        $class_name = "\\App\\Controllers\\" . $controller;
        
        if (class_exists($class_name)) {
            $objController = new $class_name();
        } else {
            new ErrorHttp(['template' => 'error_page.error']);
        }
        
        $action = $this->action;
        if (method_exists($objController, $action)) {
            
            $this->startControllerAction($objController, $action);
            
        } else {
            new ErrorHttp(['template' => 'error_page.error']);
        }
    }
    
    private function startControllerAction(ControllerInterface $controller, $action)
    {
        $controller->$action();
    }
    
    
}
