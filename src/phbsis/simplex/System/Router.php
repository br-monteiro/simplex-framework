<?php

namespace Simplex\System;

use Aura\Router\RouterFactory;
use App\Route;
use Simplex\Views\ErrorHttp;

class Router
{
    private $routerFactory;
    private $router;
    private $route;
    
    public function __construct()
    {
        $this->routerFactory = new RouterFactory();
        $router = $this->routerFactory->newInstance();
        
        $route = new Route($router);
        $this->router = $route->definedRoute();
        
        $this->configMatch();
        
        $this->failureRoute();
        
        $this->noRoute();
    }
    
    private function configMatch()
    {
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->route = $this->router->match($path, $_SERVER);
    }
    
    private function failureRoute()
    {
        $failure = $this->router->getFailedRoute();

        if ($failure) {
            // inspect the failed route
            if ($failure->failedMethod()) {
                // the route failed on the allowed HTTP methods.
                // this is a "405 Method Not Allowed" error.
                new ErrorHttp([
                        'template' => 'error_page.error',
                        'status' => 405,
                        'header' => "HTTP/1.0 405 Method Not Allowed",
                        'message' => 'the route failed on the allowed HTTP methods'
                    ]);
                
            } elseif ($failure->failedAccept()) {
                // the route failed on the available content-types.
                // this is a "406 Not Acceptable" error.
                new ErrorHttp([
                        'template' => 'error_page.error',
                        'status' => 406,
                        'header' => "HTTP/1.0 406 Not Acceptable",
                        'message' => 'the route failed on the available content-types'
                    ]);
            }
        }
    }
    
    private function noRoute()
    {
        if (!$this->route) {
            // no route object was returned
            new ErrorHttp([
                    'template' => 'error_page.error',
                    'status' => 404,
                    'header' => "HTTP/1.0 404 Not Found",
                    'message' => 'No application route was found for that URL path.'
                ]);
        }
    }
    
    public function getParams()
    {
        return $this->route->params;
    }

}
