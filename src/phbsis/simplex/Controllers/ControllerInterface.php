<?php

namespace Simplex\Controllers;

interface ControllerInterface
{
    public function indexAction();
}
