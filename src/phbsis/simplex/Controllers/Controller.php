<?php

namespace Simplex\Controllers;

use Simplex\System\Bootstrap;
use Simplex\Views\Views;

class Controller extends Bootstrap
{
    private $views;
    
    public function __construct()
    {
        parent::__construct();
        $this->views = new Views();
    }
    
    public function views()
    {
        return $this->views;
    }
}
