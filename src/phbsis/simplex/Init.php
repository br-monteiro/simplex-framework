<?php

namespace Simplex;

use Simplex\System\Bootstrap;

class Init
{
    public function __construct() {
        $app = new Bootstrap();
        $app->run();
    }
}
